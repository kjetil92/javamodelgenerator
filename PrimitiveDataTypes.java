/**
 * Created with IntelliJ IDEA.
 * User: Kjetil
 * Date: 18.10.13
 * Time: 21:50
 * To change this template use File | Settings | File Templates.
 */
public enum PrimitiveDataTypes {
    BYTE,
    SHORT,
    INT,
    LONG,
    FLOAT,
    DOUBLE,
    BOOLEAN,
    CHAR;

    public static boolean isPrimitiveDataType(String type) {
        if(type.equals(BYTE.toString().toLowerCase()))
            return true;
        else if(type.equals(SHORT.toString().toLowerCase()))
            return true;
        else if(type.equalsIgnoreCase(INT.toString()))
            return true;
        else if(type.equals(LONG.toString().toLowerCase()))
            return true;
        else if(type.equals(FLOAT.toString().toLowerCase()))
            return true;
        else if(type.equals(DOUBLE.toString().toLowerCase()))
            return true;
        else if(type.equals(CHAR.toString().toLowerCase()))
            return true;
        return false;
    }
}

