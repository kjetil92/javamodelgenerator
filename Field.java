/**
 * Created with IntelliJ IDEA.
 * User: Kjetil
 * Date: 18.10.13
 * Time: 18:56
 * To change this template use File | Settings | File Templates.
 */
public class Field {
    private String name;
    private String primitiveDataTypeOrClass;
    private String defaultValue;
    private String defaultValueString;
    private byte defaultValueByte;
    private short defaultValueShort;
    private int defaultValueInt;

    public Field(String name, String primitiveDataTypeOrClass) {
        this.name = name;
        this.primitiveDataTypeOrClass = primitiveDataTypeOrClass;
        this.defaultValue = "";
    }

    public Field(String name, String primitiveDataTypeOrClass, String defaultValue) {
        this.name = name;
        this.primitiveDataTypeOrClass = primitiveDataTypeOrClass;
        this.defaultValue = defaultValue;
    }

    public void setDefaultValue() {
        if(primitiveDataTypeOrClass.equals("String"))
            defaultValueString = defaultValue;
        else if(primitiveDataTypeOrClass.equals("byte"))
            defaultValueByte = Byte.parseByte(defaultValue);
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrimitiveDataTypeOrClass(String primitiveDataTypeOrClass) {
        this.primitiveDataTypeOrClass = primitiveDataTypeOrClass;
    }

    public String getName() {
        return this.name;
    }

    public String getPrimitiveDataTypeOrClass() {
        return this.primitiveDataTypeOrClass;
    }

    public String getDefaultValue() {
        return this.defaultValue;
    }
}
