import java.io.*;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Kjetil
 * Date: 18.10.13
 * Time: 18:43
 * To change this template use File | Settings | File Templates.
 */
public class GenerateModel {
    private String modelName;
    private ArrayList<Field> modelFields;
    private boolean isFinishingMakingClass = false;

    /**
     *
     * @param modelName
     * @param fields
     */
    public GenerateModel(String modelName, String... fields) {
        this.modelName = modelName;
        modelFields = new ArrayList<Field>(fields.length);
        // Adds the specified fields to the fields list of the generator
        for(String field : fields) {
            String[] strings = field.split(":");

            if(strings.length == 2) {
                modelFields.add(new Field(strings[0], strings[1]));
            } else if(strings.length < 2) {

            } else if(strings.length == 3) {
                modelFields.add(new Field(strings[0], strings[1], strings[2]));
            }
        }


        validateNonePrimitiveDataTypes();
        validateIdFields();

        generateClass();
    }

    public void generateClass() {
        if(modelName.charAt(0) >= 65 && modelName.charAt(0) <= 90) {
            try {
                PrintWriter pw = new PrintWriter(new FileOutputStream(modelName + ".java", true));

                if(!isFinishingMakingClass) {
                    pw.println("/**");
                    pw.println(" * " + modelName + " class");
                    pw.println(" * ");
                    pw.println(" * @author Auto-Generated Android Model Class");
                    pw.println(" * @version 1.0.0");
                    pw.println(" */");

                    pw.println("public class " + modelName + " {");
                    pw.println("\tprivate long id;");

                    for(Field field : modelFields) {
                        if(field.getDefaultValue().equals(""))
                            pw.println("\tprivate " + field.getPrimitiveDataTypeOrClass() + " " + field.getName() + ";");
                        else
                            pw.println("\tprivate " + field.getPrimitiveDataTypeOrClass() + " " + field.getName() +
                                " = " + field.getDefaultValue() + ";");
                    }

                    generateConstructors(pw);
                    generateSetMethods(pw);
                    generateGetMethods(pw);
                    generateToStringMethod(pw);
                    generateEqualsMethod(pw);
                    pw.println("}\n");
                }
                pw.close();

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        } else {
        
        }
    }

    public void generateConstructors(PrintWriter pw) {
        for(int i = 0; i < 2; i++) {
            if(i == 0) {
                pw.print("\n\tpublic " + modelName + "(");

                for(Field field : modelFields) {
                    Field f = modelFields.get(modelFields.size()-1);

                    if(!field.getName().equals(f.getName()))
                        pw.print(field.getPrimitiveDataTypeOrClass() + " " + field.getName() + ", ");
                    else
                        pw.print(field.getPrimitiveDataTypeOrClass() + " " + field.getName() + ") {\n");
                }

                for(Field field : modelFields) {
                    pw.println("\t\tthis." + field.getName() + " = " + field.getName() + ";");
                }
                pw.println("\t}");
            } else {
                pw.print("\n\tpublic " + modelName + "(long id, ");

                for(Field field : modelFields) {
                    Field f = modelFields.get(modelFields.size()-1);

                    if(!field.getName().equals(f.getName()))
                        pw.print(field.getPrimitiveDataTypeOrClass() + " " + field.getName() + ", ");
                    else
                        pw.print(field.getPrimitiveDataTypeOrClass() + " " + field.getName() + ") {\n");
                }

                pw.println("\t\tthis.id = id;");
                for(Field field : modelFields) {
                    pw.println("\t\tthis." + field.getName() + " = " + field.getName() + ";");
                }
                pw.println("\t}");
            }
        }
    }

    public void generateGetMethods(PrintWriter pw) {
        pw.println("\n\t/**");
        pw.println("\t * Method to get id");
        pw.println("\t * @return id");
        pw.println("\t */");
        pw.println("\tpublic long getId() {");
        pw.println("\t\treturn this.id;");
        pw.println("\t}");

        for(Field field : modelFields) {
            pw.println("\n\t/**");
            pw.println("\t * Method to get " + field.getName());
            pw.println("\t * @return " + field.getName());
            pw.println("\t */");
            pw.println("\tpublic " + field.getPrimitiveDataTypeOrClass() + " get" + firstCharToUpperCase(field.getName()) + "() {");
            pw.println("\t\treturn this." + field.getName() + ";");
            pw.println("\t}");
        }
    }

    public void generateSetMethods(PrintWriter pw) {
        pw.println("\n\t/**");
        pw.println("\t * Method to set id");
        pw.println("\t * @param id");
        pw.println("\t */");
        pw.println("\tpublic void setId(long id) {");
        pw.println("\t\tthis.id = id;");
        pw.println("\t}");

        for(Field field : modelFields) {
            pw.println("\n\t/**");
            pw.println("\t * Method to set " + field.getName());
            pw.println("\t * @param " + field.getName());
            pw.println("\t */");
            pw.println("\tpublic void set" + firstCharToUpperCase(field.getName()) + "(" + field.getPrimitiveDataTypeOrClass() + " "
                    + field.getName() + ") {");
            pw.println("\t\tthis." + field.getName() + " = " + field.getName() + ";");
            pw.println("\t}");
        }
    }

    public void generateToStringMethod(PrintWriter pw) {
        pw.println("\n\t/**");
        pw.println("\t * Method that returns a String representation of " + modelName);
        pw.println("\t * @return - String representation of " + modelName);
        pw.println("\t */ ");
        pw.println("\t@Override");
        pw.println("\tpublic String toString() {");
        pw.print("\t\treturn \"LineItem with id: \" + id + \"");

        for(Field field : modelFields) {
            pw.print(" " + field.getName() + ": \" + " + field.getName() + " + \"");
        }
        pw.println("\";\n\t}");
    }

    public void generateToJsonMethod(PrintWriter pw) {

    }

    public void generateEqualsMethod(PrintWriter pw) {
        pw.println("\n\t/**");
        pw.println("\t * Method that checks if " + modelName + "1 and " + modelName + "2 is equal");
        pw.println("\t * @return <code>true</code> - if they are equal</br> <code>false</code> - if they are not");
        pw.println("\t */");
        pw.println("\t@Override");
        pw.println("\tpublic boolean equals(" + modelName + " " + firstCharToLowerCase(modelName) + ") {");
        pw.println("\t\treturn (id == " + firstCharToLowerCase(modelName) + ".getId());");
        pw.println("\t}");
    }

    /**
     * Id fields e.g fields that will end with "id"
     * should have the datatype of long.
     */
    public void validateIdFields() {
        for(Field field : modelFields) {
            if(field.getName().endsWith("Id")) {
                if(!field.getPrimitiveDataTypeOrClass().equals("long")) {
                    System.out.println("The datatype of " + field.getName() + " should be long but is " + field.getPrimitiveDataTypeOrClass() +
                        " setting it automatically to long");
                    field.setPrimitiveDataTypeOrClass("long");
                }
            }
        }
    }

    /**
     * None primitive datatypes need to be spellchecked
     */
    public void validateNonePrimitiveDataTypes() {
        for(Field field : modelFields) {
            if(!PrimitiveDataTypes.isPrimitiveDataType(field.getPrimitiveDataTypeOrClass())) {
                if(!isUpperCase(field.getPrimitiveDataTypeOrClass().charAt(0))) {
                    System.out.println("First letter in class name not uppercase correcting the problem");
                    String newDataType = "";
                    for(int i = 0; i < field.getPrimitiveDataTypeOrClass().length(); i++) {
                        if(i == 0) {
                            char chr = (char) (field.getPrimitiveDataTypeOrClass().charAt(0)-32);
                            System.out.println(chr);
                            newDataType += chr;
                        } else {
                            newDataType += field.getPrimitiveDataTypeOrClass().charAt(i);
                        }
                    }
                    field.setPrimitiveDataTypeOrClass(newDataType);
                }

            }
        }
    }

    public boolean isUpperCase(char chr) {
        return (chr >= 'A' && chr <= 'Z');
    }

    public String firstCharToUpperCase(String word) {
        String temp = "";
        for(int i = 0; i < word.length(); i++) {
            if(i == 0 && !isUpperCase(word.charAt(0)))
                temp += (char) (word.charAt(0)-32);
            else temp += word.charAt(i);
        }
        return temp;
    }

    public String firstCharToLowerCase(String word) {
        String temp = "";
        for(int i = 0; i < word.length(); i++) {
            if(i == 0)
                temp += (char) (word.charAt(0)+32);
            else temp += word.charAt(i);
        }
        return temp;
    }

    public String getModelName() {
        return this.modelName;
    }

    public ArrayList<Field> getModelFields() {
        return this.modelFields;
    }
}
